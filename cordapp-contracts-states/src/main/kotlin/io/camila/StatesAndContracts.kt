package io.camila


import com.fasterxml.jackson.annotation.JsonValue
import net.corda.core.contracts.*
import net.corda.core.contracts.Requirements.using
import net.corda.core.crypto.NullKeys
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.CordaSerializable
import net.corda.core.transactions.LedgerTransaction
import java.lang.IllegalArgumentException
import java.security.PublicKey
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Index
import javax.persistence.Table

/**
 * The state object recording CRM assets between two parties.
 *
 * A state must implement [AccountState] or one of its descendants.
 *
 * @Param name of the Account.
 * @param type of the Account.
 * @param industry of the Account.
 * @param rating of the Account.
 * @param phone of the Account.
 * @param owner the party who owns the Account.
 */


@CordaSerializable
data class Account(val accountId: String,
                   val accountName: String,
                   val accountType: String,
                   val industry: String,
        //   val rating: AccountRating,
                   val phone: String,
                   override val owner: AbstractParty
) : OwnableState {
    override val participants: List<AbstractParty> = listOf(owner)

    // override fun supportedSchemas(): Iterable<MappedSchema> = setOf(AccountSchemaV1)


    fun withoutOwner() = copy(owner = NullKeys.NULL_PARTY)

    override fun withNewOwner(newOwner: AbstractParty): CommandAndState {
        return CommandAndState(AccountContract.Commands.Transfer(), this.copy(owner = newOwner))
    }

    /*
    override fun generateMappedObject(schema: MappedSchema): PersistentState {
        return when (schema) {
            is AccountSchemaV1 -> AccountSchemaV1.PersistentAccount(
                    accountName = this.accountName.toString(),
                    type = this.type,
                    industry = this.industry,
                    phone = this.phone,
                    rating = this.rating.toString(),
                    owner = this.owner,
                    accountId = this.accountId.toString()
            )
            else -> throw IllegalArgumentException("Unrecognised schema $schema")
        }
    }

*/


    @CordaSerializable
    data class AccountProperties(
            val accountId: String,
            val accountName: String,
            val accountType: String,
            val industry: String,
            val phone: String,
            val owner: String
    )


    enum class AccountRating {
        HOT, WARM, COLD
    }

}



// **********************
// * Attachment Contract *
// **********************

class AttachmentContract : Contract {
    override fun verify(tx: LedgerTransaction) {
        val state = tx.outputsOfType<AttachmentContract.Attachment>().single()
        // we check that at least one has the matching hash, the other will be the contract
        require(tx.attachments.any { it.id == state.hash })
    }

    object Command : TypeOnlyCommandData()

    data class Attachment(val hash: SecureHash.SHA256) : ContractState {
        override val participants: List<AbstractParty> = emptyList()
    }
}





// *****************
// * Contract Code *
// *****************

class AccountContract : Contract {
    companion object {
        @JvmStatic
        val ACCOUNT_CONTRACT_ID = "dapps.crm.AccountContract"
    }

    interface Commands : CommandData {
        class Create : TypeOnlyCommandData(), Commands
        class Transfer : TypeOnlyCommandData(), Commands


    }

    override fun verify(tx: LedgerTransaction) {
        val command = tx.commands.requireSingleCommand<Commands>()
        val setOfSigners = command.signers.toSet()
        when (command.value) {
            is Commands.Create -> verifyCreate(tx, setOfSigners)
            is Commands.Transfer -> verifyTransfer(tx, setOfSigners)
            else -> throw IllegalArgumentException("Unrecognised command.")
        }
    }

    private fun verifyCreate(tx: LedgerTransaction, signers: Set<PublicKey>) = requireThat {
        "No inputs must be consumed." using (tx.inputStates.isEmpty())
        "Only one out state should be created." using (tx.outputStates.size == 1)
        val output = tx.outputsOfType<Account>().single()
        "Owner only may sign the Account issue transaction." using (output.owner.owningKey in signers)
    }

    private fun verifyTransfer(tx: LedgerTransaction, signers: Set<PublicKey>) = requireThat {
        val inputAccounts = tx.inputsOfType<Account>()
        //val inputAccountTransfers = tx.inputsOfType<AccountTransfer>()
        "There must be one input Account." using (inputAccounts.size == 1)


        val inputAccount = inputAccounts.single()
        val outputs = tx.outputsOfType<Account>()
        "There must be one output Account." using (outputs.size == 1)


        val output = outputs.single()
        "Must not not change Account data except owner field value." using (inputAccount == output.copy(owner = inputAccount.owner))
        "Owner only may sign the Account transfer transaction." using (output.owner.owningKey in signers)
    }
}





/**
 * The state object recording CRM assets between two parties.
 *
 * A state must implement [ContactState] or one of its descendants.
 *
 * @Param contactId of the Contact.
 * @Param firstName of the Contact.
 * @Param lastName of the Contact.
 * @param email of the Contact.
 * @param phone of the Contact.
 * @param owner the party who owns the Contact.
 */

@CordaSerializable
data class Contact(val contactId: String,
                   val firstName: String,
                   val lastName: String,
                   val email: String,
                   val phone: String,
                   override val owner: AbstractParty
) :
        QueryableState, OwnableState {

    override val participants: List<AbstractParty> = listOf(owner)
    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(ContactSchemaV1)


    fun withoutOwner() = copy(owner = NullKeys.NULL_PARTY)

    override fun withNewOwner(newOwner: AbstractParty): CommandAndState {
        return CommandAndState(ContactContract.Commands.Transfer(), this.copy(owner = newOwner))
    }

    override fun generateMappedObject(schema: MappedSchema): PersistentState {
        return when (schema) {
            is ContactSchemaV1 -> ContactSchemaV1.PersistentContact(
                    contactId = this.contactId,
                    firstName = this.firstName,
                    lastName = this.lastName,
                    email = this.email.toString(),
                    phone = this.phone,
                    owner = this.owner
            )
            else -> throw IllegalArgumentException("Unrecognised schema $schema")
        }
    }


    @CordaSerializable
    data class ContactProperties(
            val owner: AbstractParty,
            val firstName: String,
            val lastName: String,
            val email: String,
            val industry: String,
            val phone: String,
            val contactId: String
    )

}



class ContactContract : Contract {
    companion object {
        @JvmStatic
        val CONTACT_CONTRACT_ID = "dapps.crm.ContactContract"
    }

    interface Commands : CommandData {
        class Create : TypeOnlyCommandData(), Commands
        class Transfer : TypeOnlyCommandData(), Commands
        class Share : TypeOnlyCommandData(), Commands
        class Erase : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction) {
        val command = tx.commands.requireSingleCommand<Commands>()
        val setOfSigners = command.signers.toSet()
        when (command.value) {
            is Commands.Create -> verifyCreate(tx, setOfSigners)
            is Commands.Transfer -> verifyTransfer(tx, setOfSigners)
            else -> throw IllegalArgumentException("Unrecognised command.")
        }
    }

    private fun verifyCreate(tx: LedgerTransaction, signers: Set<PublicKey>) = requireThat {
        "No inputs must be consumed." using (tx.inputStates.isEmpty())
        "Only one out state should be created." using (tx.outputStates.size == 1)
        val output = tx.outputsOfType<Contact>().single()
        "Owner only may sign the Account issue transaction." using (output.owner.owningKey in signers)
    }

    private fun verifyTransfer(tx: LedgerTransaction, signers: Set<PublicKey>) = requireThat {
        val inputContacts = tx.inputsOfType<Contact>()
        //val inputContactTransfers = tx.inputsOfType<ContactTransfer>()
        "There must be one input Contact." using (inputContacts.size == 1)


        val inputContact = inputContacts.single()
        val outputs = tx.outputsOfType<Contact>()
        // If the obligation has been partially settled then it should still exist.
        "There must be one output Contact." using (outputs.size == 1)

        // Check only the paid property changes.
        val output = outputs.single()
        "Must not not change Contact data except owner field value." using (inputContact == output.copy(owner = inputContact.owner))
        "Owner only may sign the Contact transfer transaction." using (output.owner.owningKey in signers)
    }
}




/**
 * The family of schemas for [ContactSchema].
 */

object ContactSchema

/**
 * First version of an [ContactSchema] schema.
 */


object ContactSchemaV1 : MappedSchema(ContactSchema.javaClass, 1, listOf(PersistentContact::class.java)) {
    @Entity
    @Table(name = "contacts", indexes = arrayOf(Index(name = "idx_contact_owner", columnList = "owner"),
            Index(name = "idx_contact_lastName", columnList = "lastName")))
    class PersistentContact(
            @Column(name = "firstName")
            var firstName: String,

            @Column(name = "lastName")
            var lastName: String,

            @Column(name = "phone")
            var phone: String,

            @Column(name = "email")
            var email: String,

            @Column(name = "owner")
            var owner: AbstractParty,

            @Column(name = "contactId")
            var contactId: String

    ) : PersistentState() {
        constructor() : this("default-constructor-required-for-hibernate", "", "", "", NullKeys.NULL_PARTY, "")
    }

}



class LeadContract : Contract {
    companion object {
        @JvmStatic
        val LEAD_CONTRACT_ID = "dapps.crm.LeadContract"
    }

    interface Commands : CommandData {
        class Create : TypeOnlyCommandData(), Commands
        class Transfer : TypeOnlyCommandData(), Commands
        class Share : TypeOnlyCommandData(), Commands
        class Erase : TypeOnlyCommandData(), Commands
        class Convert : TypeOnlyCommandData(), Commands
    }

    override fun verify(tx: LedgerTransaction) {
        val command = tx.commands.requireSingleCommand<Commands>()
        val setOfSigners = command.signers.toSet()
        when (command.value) {
            is Commands.Create -> verifyCreate(tx, setOfSigners)
            is Commands.Transfer -> verifyTransfer(tx, setOfSigners)
            else -> throw IllegalArgumentException("Unrecognised command.")
        }
    }

    private fun verifyCreate(tx: LedgerTransaction, signers: Set<PublicKey>) = requireThat {
        "No inputs must be consumed." using (tx.inputStates.isEmpty())
        "Only one out state should be created." using (tx.outputStates.size == 1)
        val output = tx.outputsOfType<Lead>().single()
        "Owner only may sign the Account issue transaction." using (output.owner.owningKey in signers)
    }

    private fun verifyTransfer(tx: LedgerTransaction, signers: Set<PublicKey>) = requireThat {
        val inputLeads = tx.inputsOfType<Lead>()
        val inputLeadTransfers = tx.inputsOfType<Lead>()
        "There must be one input obligation." using (inputLeads.size == 1)


        val inputLead = inputLeads.single()
        val outputs = tx.outputsOfType<Lead>()
        // If the obligation has been partially settled then it should still exist.
        "There must be one output Lead." using (outputs.size == 1)

        // Check only the paid property changes.
        val output = outputs.single()
        "Must not not change Lead data except owner field value." using (inputLead == output.copy(owner = inputLead.owner))
        "Owner only may sign the Lead issue transaction." using (output.owner.owningKey in signers)
    }
}


// *********
// * State *
// *********


/**
 * The state object recording CRM assets between two parties.
 *
 * A state must implement [LeadState] or one of its descendants.
 *
 * @Param firstName of the Lead.
 * @Param lastName of the Lead.
 * @param email of the Lead.
 * @param phone of the Lead.
 * @param status of the Lead.
 * @param owner the party who owns the Lead.
 */
@CordaSerializable
data class Lead(val leadId: String,
                val firstName: String,
                val lastName: String,
                val email: String,
                val phone: String,
        //    val leadStatus: LeadStatus,
                override val owner: AbstractParty ) :
        QueryableState, OwnableState {



    override val participants: List<AbstractParty> = listOf(owner)
    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(LeadSchemaV1)

    fun withoutOwner() = copy(owner = NullKeys.NULL_PARTY)
    //fun working() = copy(status = LeadStatus.WORKING)
    //fun closedConverted() = copy(status = LeadStatus.CLOSED_CONVERTED)
    //fun closedNotConverted() = copy(status = LeadStatus.CLOSED_NONCONVERTED)

    override fun withNewOwner(newOwner: AbstractParty): CommandAndState {
        return CommandAndState(LeadContract.Commands.Transfer(), this.copy(owner = newOwner))
    }

    override fun generateMappedObject(schema: MappedSchema): PersistentState {
        return when (schema) {
            is LeadSchemaV1 -> LeadSchemaV1.PersistentLead(
                    leadId = this.leadId,
                    firstName = this.firstName,
                    lastName = this.lastName,
                    email = this.email,
                    phone = this.phone,
                    //  leadStatus = this.leadStatus.toString(),
                    owner = this.owner
            )
            else -> throw IllegalArgumentException("Unrecognised schema $schema")
        }
    }


    @CordaSerializable
    data class LeadProperties(
            val firstName: String,
            val lastName: String,
            val email: String,
            val status: String,
            val phone: String,
            val owner: String
    )


    @CordaSerializable
    enum class LeadStatus {
        OPEN,
        WORKING,
        CLOSED_CONVERTED,
        CLOSED_NONCONVERTED
    }


    @CordaSerializable
    enum class RequestStatus(@JsonValue val value: String) {
        PENDING_CONFIRMATION("Pending Confirmation"), //Initial status
        PENDING("Pending"), // updated by buyer
        TRANSFERRED("Transferred"), // on valid asset data clearing house update this status
        REJECTED("Rejected"), // on invalid asset data clearing house reject transaction with this status.
        FAILED("Failed") // on fail of settlement e.g. with insufficient cash from Buyer party.
    }

}


// *********
// * Schema *
// *********



object LeadSchema

/**
 * First version of an [LeadSchema] schema.
 */


object LeadSchemaV1 : MappedSchema(schemaFamily = LeadSchema.javaClass, version = 1, mappedTypes = listOf(PersistentLead::class.java)) {
    @Entity
    @Table(name = "leads", indexes = arrayOf(Index(name = "idx_lead_owner", columnList = "owner"),
            Index(name = "idx_lead_lastName", columnList = "lastName")))
    class PersistentLead(
            @Column(name = "firstName")
            var firstName: String,

            @Column(name = "lastName")
            var lastName: String,

            @Column(name = "phone")
            var phone: String,

            @Column(name = "email")
            var email: String,

            // @Column(name = "status")
            //  var leadStatus: String,

            @Column(name = "owner")
            var owner: AbstractParty,

            @Column(name = "leadId")
            var leadId: String


    ) : PersistentState() {
        constructor() : this("default-constructor-required-for-hibernate", "", "", "",  NullKeys.NULL_PARTY, "")
    }
}



// *****************
// * Contract Code *
// *****************

class CaseContract : Contract {

    companion object {
        val ID = "com.template.TemplateContract"
    }

    override fun verify(tx: LedgerTransaction) {
        val caseInputs = tx.inputsOfType<Case>()
        val caseOutputs = tx.outputsOfType<Case>()
        val caseCommand = tx.commandsOfType<CaseContract.Commands>().single()

        when(caseCommand.value) {
            is Commands.SubmitCase -> requireThat {
                "no inputs should be consumed" using (caseInputs.isEmpty())
                // TODO we might allow several jobs to be proposed at once later
                "one output should be produced" using (caseOutputs.size == 1)

                val caseOutput = caseOutputs.single()
                "the submitter should be different to the resolver" using (caseOutput.resolver != caseOutput.submitter)
                //  "the status should be set as unstarted" using (caseOutput.caseStatus == CaseStatus.UNSTARTED)

                "the resolver and submitter are required signer" using
                        (caseCommand.signers.containsAll(listOf(caseOutput.resolver.owningKey, caseOutput.submitter.owningKey)))
            }

            is Commands.StartCase -> requireThat {
                "one input should be consumed" using (caseInputs.size == 1)
                "one output should bbe produced" using (caseOutputs.size == 1)

                val caseInput = caseInputs.single()
                val caseOutput = caseOutputs.single()
                // "the status should be set to started" using (caseOutput.caseStatus == CaseStatus.STARTED)
                //  "the previous status should not be STARTED" using (caseInput.caseStatus != CaseStatus.STARTED)
                //  "only the job status should change" using (caseOutput == caseInput.copy(caseStatus = CaseStatus.STARTED))
                "the submitter and resolver are required signers" using
                        (caseCommand.signers.containsAll(listOf(caseOutput.resolver.owningKey, caseOutput.submitter.owningKey)))
            }

            is Commands.CloseCase -> requireThat {
                "one input should be produced" using (caseInputs.size == 1)
                "one output should be produced" using (caseOutputs.size == 1)

                val caseInput = caseInputs.single()
                val caseOutput = caseOutputs.single()

                //    "the input status must be set as started" using (caseInputs.single().caseStatus == CaseStatus.STARTED)
                //   "the output status should be set as finished" using (caseOutputs.single().caseStatus == CaseStatus.CLOSED)
                //   "only the status must change" using (caseInput.copy(caseStatus = CaseStatus.CLOSED) == caseOutput)
                "the update must be signed by the contractor of the " using (caseOutputs.single().submitter== caseInputs.single().submitter)
                "the submitter should be signer" using (caseCommand.signers.contains(caseOutputs.single().submitter.owningKey))

            }

            is Commands.EscalateCase -> requireThat {

            }

            is Commands.CloseOutOfScopeCase -> requireThat {
                // This insures we only have one input and one output
                val caseOutput = caseOutputs.single()
                val caseInput = caseInputs.single()

                "Only status should have changed" using (caseOutput.submitter == caseInput.submitter
                        && caseOutput.resolver == caseInput.resolver
                        && caseOutput.description == caseInput.description)
                //     "Status should show rejected" using (caseOutput.caseStatus == CaseStatus.OUTOFSCOPE)
                //      "Job must have been previously started" using (caseInput.caseStatus == CaseStatus.STARTED)

                "Resolver should be a signer" using (caseCommand.signers.contains(caseOutput.resolver.owningKey))
            }

            else -> throw IllegalArgumentException("Unrecognised command.")
        }
    }

    // Used to indicate the transaction's intent.
    interface Commands : CommandData {
        class SubmitCase : Commands
        class StartCase : Commands
        class CloseCase : Commands
        class EscalateCase : Commands
        class CloseOutOfScopeCase : Commands

    }
}

// *********
// * State *
// *********
@CordaSerializable
data class Case(val caseId: String,
                val description: String,
                val caseNumber: String,
                val caseStatus: CaseStatus,
                val priority: Priority,
                val submitter: Party,
                val resolver: Party,
                override val linearId: UniqueIdentifier = UniqueIdentifier()) : LinearState {
    override val participants = listOf(submitter, resolver)



    //fun working() = copy(status = CaseStatus.WORKING)
    //fun escalated() = copy(status = CaseStatus.ESCALATED)
    //fun closed() = copy(status = CaseStatus.CLOSED)
    // fun outOfScope() = copy(status = CaseStatus.OUTOFSCOPE)
}

enum class CaseStatus {
    NEW, UNSTARTED, STARTED, WORKING, ESCALATED, CLOSED, OUTOFSCOPE
}

enum class Priority {
    HIGH, MEDIUM, LOW

}



// *********
// * Schema *
// *********



object CaseSchema

/**
 * First version of an [LeadSchema] schema.
 */


object CaseSchemaV1 : MappedSchema(schemaFamily = CaseSchema.javaClass, version = 1, mappedTypes = listOf(PersistentCase::class.java)) {
    @Entity
    @Table(name = "leads", indexes = arrayOf(Index(name = "idx_case_owner", columnList = "owner"),
            Index(name = "idx_case_caseId", columnList = "caseId")))
    class PersistentCase(
            @Column(name = "caseId")
            var caseId: String,

            @Column(name = "description")
            var description: String,

            @Column(name = "caseNumber")
            var caseNumber: String,

            @Column(name = "status")
            var status: String,

            @Column(name = "priority")
            var priority: String,

            @Column(name = "submitter")
            var submitter: String,

            @Column(name = "resolver")
            var resolver: String,

            @Column(name = "linearId")
            var linearId: UUID


    ) : PersistentState() {
        constructor() : this("", "", "", "", "", "", "", java.util.UUID.randomUUID())
    }
}



const val ACCOUNT_CONTRACT_ID = "com.template.AccountContract"
const val CONTACT_CONTRACT_ID = "com.template.ContactContract"
const val LEAD_CONTRACT_ID = "com.template.LeadContract"
const val CASE_CONTRACT_ID = "com.template.CaseContract"






@CordaSerializable
data class AccountProperties(
        val accountId: String,
        val accountName: String,
        val accountType: String,
        val industry: String,
        val phone: String,
        //  val rating: String,
        val owner: String
)

@CordaSerializable
data class ContactProperties(
        val contactId: String,
        val firstName: String,
        val lastName: String,
        val phone: String,
        val email: String,
        val owner: String
)

@CordaSerializable
data class LeadProperties(
        val leadId: String,
        val firstName: String,
        val lastName: String,
        val phone: String,
        val email: String,
        val owner: String
)

































